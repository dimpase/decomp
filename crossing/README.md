# Bounding the crossing number of K\_{m,n}

This directory contains code to compute alpha\_m, the constant defined
in this paper: https://homepages.cwi.nl/~lex/files/symm.pdf which is
involved in bounding the crossing number of K\_{m,n} and Zarankiewicz's
crossing number conjecture.

## Running the code

Install Sage and GAP, then run e.g:

    $ ./crossing.sage 3

to compute alpha\_3. Computing anything larger than alpha\_9 will
probably never terminate.
