#
# RepnDecomp: Decompose representations of finite groups into irreducibles
#
# Reading the declaration part of the package.
#

ReadPackage( "RepnDecomp", "lib/utils.gd" );
ReadPackage( "RepnDecomp", "lib/serre.gd" );
ReadPackage( "RepnDecomp", "lib/centralizer.gd" );
ReadPackage( "RepnDecomp", "lib/block_diagonalize.gd" );
ReadPackage( "RepnDecomp", "lib/filters.gd" );
