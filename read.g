#
# RepnDecomp: Decompose representations of finite groups into irreducibles
#
# Reading the implementation part of the package.
#

ReadPackage( "RepnDecomp", "lib/utils.gi");
ReadPackage( "RepnDecomp", "lib/serre.gi");
ReadPackage( "RepnDecomp", "lib/centralizer.gi");
ReadPackage( "RepnDecomp", "lib/block_diagonalize.gi");
ReadPackage( "RepnDecomp", "lib/filters.gi");
