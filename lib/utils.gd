DeclareGlobalFunction( "BlockDiagonalMatrix" );
DeclareGlobalFunction( "Replicate" );
DeclareGlobalFunction( "TensorProductRepLists" );
DeclareGlobalFunction( "ComposeHomFunction" );
DeclareGlobalFunction( "DirectSumRepList" );
DeclareGlobalFunction( "AreRepsIsomorphic" );
DeclareGlobalFunction( "DegreeOfRepresentation" );
